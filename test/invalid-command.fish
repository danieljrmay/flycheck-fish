#!/usr/bin/fish
echo "This fish script contains an invalid command."

# The following line contains an invalid command which should be
# flagged by flycheck-fish
invalid-command
