;;; flycheck-fish.el --- Flycheck integration for Fish

;; Copyright 2020 Daniel J. R. May

;; Author: Daniel J. R. May <daniel.may@danieljrmay.com>
;; URL: https://gitlab.com/danieljrmay/flycheck-fish
;; Package-Requires: ((flycheck "31"))
;; Created: 07 December 2020
;; Version: 0.1
;; Keywords: convenience, languages, tools

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see
;; <https://www.gnu.org/licenses/>.

;;; Commentary:
;; This file provides a very basic syntax checker for fish scripts via
;; the fish --no-execute command and flycheck.  Obviously fish needs
;; to be installed for this to work.

;; Find out more about flycheck and how to intstall it at
;; <https://www.flycheck.org>.

;; If you are developing this code then you will want to have a look
;; at the flycheck developer documetation at
;; <https://www.flycheck.org/en/latest/developer/developing.html>.

;;; Code:

(require 'flycheck)

(defvar flycheck-fish-command (executable-find
                               (concat
                                (file-name-directory (or load-file-name buffer-file-name))
                                "bin/flycheck-fish"))
  "The command which is run by flycheck and whose output is parsed to obtain any error messages.")

(flycheck-define-command-checker 'fish
  "A fish script syntax checker.

This runs the flycheck-fish command which is an executable script
normally installed at /usr/bin/flycheck-fish on the current
source file and interprets the error messages for flycheck.

See URL https://gitlab.com/danieljrmay/flycheck-fish."
  :command `(,flycheck-fish-command
             source)
  :error-patterns
  '((error line-start
          (file-name (one-or-more (not " " ))) " (line "
          line "): \n"
          (message (one-or-more not-newline))
          (one-or-more anychar)
          ))
  :modes 'fish-mode)

(setq flycheck-check-syntax-automatically '(mode-enabled save))

;;;###autoload
(defun flycheck-fish-setup()
  "Setup Flycheck Fish integration."
  (interactive)
  (add-to-list 'flycheck-checkers 'fish))

(provide 'flycheck-fish)
;;; flycheck-fish.el ends here

