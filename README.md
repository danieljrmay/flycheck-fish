<!-- markdownlint-disable MD033 -->
# <img src="images/logo.png" width="100" alt="Flycheck Fish logo"/> Flycheck Fish

[![pipeline status](https://gitlab.com/danieljrmay/flycheck-fish/badges/master/pipeline.svg)](https://gitlab.com/danieljrmay/flycheck-fish/commits/master)
[![Copr build status](https://copr.fedorainfracloud.org/coprs/danieljrmay/emacs-flycheck-fish/package/emacs-flycheck-fish/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/danieljrmay/emacs-flycheck-fish/package/emacs-flycheck-fish/)

Ease the development of your [Fish](https://fishshell.com/) scripts in
[Emacs](https://www.gnu.org/software/emacs/) with this simple
[Flycheck](https://www.flycheck.org/) syntax checker.

![Screenshot of Flycheck Fish in action.](images/screenshot.png
"Flycheck Fish in action, notice the red underlining of the invalid command.")

## Installation

You currently have two installation options: use a Copr repository or
install from source.

### Install via Copr Repository

If you are running a RPM based Linux distribution (e.g. Fedora,
RedHat, CentOS, openSUSE, Mageia, etc.) then you can get this package
via a [fedora copr
repository](https://copr.fedorainfracloud.org/coprs/danieljrmay/emacs-flycheck-fish/).
That means you can install it with something as simple as:

```shell
sudo dnf copr enable danieljrmay/emacs-flycheck-fish
sudo dnf install emacs-flycheck-fish
```

If you require a build which can be provided by fedora copr but is not
enabled for the `emacs-flycheck-fish` RPM then [create an
issue](https://gitlab.com/danieljrmay/flycheck-fish/-/issues) and I
will try and turn it on for you! 😃

### Install From Source via Makefile

Before you start you will want to make sure that you already have
[Fish](https://fishshell.com/),
[Emacs](https://www.gnu.org/software/emacs/),
[Make](https://www.gnu.org/software/make/) and
[Flycheck](https://www.flycheck.org/) installed and working on your
system.

Get the source code, either by cloning the [git
repository](https://gitlab.com/danieljrmay/flycheck-fish) or by
downloading one of the
[releases](https://gitlab.com/danieljrmay/flycheck-fish/-/releases).

Once you have got the sources you can build and install them with the
traditional:

```shell
make
sudo make install
```

However, you might want to override some of the installation paths if they are
different from the defaults on your system. You can do that with
something like this:

```shell
sudo make install MAKEVARIABLE=MyNewValue
```
