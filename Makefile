#!/usr/bin/make -f
#
# flycheck-fish GNU Makefile
#
# See: https://www.gnu.org/software/make/manual
#
# Copyright (c) 2020 Daniel J. R. May
#

#################################
### Project related variables ###
#################################

name:=flycheck-fish

spec_file:=emacs-$(name).spec
version:=$(shell awk '/Version:/ { print $$2 }' $(spec_file))
dist_name:=$(name)-$(version)
dist_tree_ish:=$(version)
tarball:=$(dist_name).tar.gz
testdestdir:=testdestdir
requirements:=git gzip mock rpmlint

# Mock variables
DISTRO:=fedora
DISTPREFIX:=.fc
RELEASEVER:=$(shell rpm --eval '%{fedora}')
ARCH:=$(shell rpm --eval '%_arch')
MOCKROOT=$(DISTRO)-$(RELEASEVER)-$(ARCH)
MOCKRESULTDIR=.
RPMFILE:=$(shell rpmspec --query --define='dist $(DISTPREFIX)$(RELEASEVER)' --target=$(ARCH) $(spec_file)).rpm
SRPMFILE:=$(subst noarch,src,$(RPMFILE))
SRCS_EL=$(name).el
OBJS_ELC=$(SRCS_EL:.el=.elc)

###############################
# Standard makefile variables #
###############################

# Commands
DNF_INSTALL=/usr/bin/dnf --assumeyes install
EMACS=/usr/bin/emacs
GIT=/usr/bin/git
GZIP=/usr/bin/gzip
INSTALL=/usr/bin/install
INSTALL_DATA=$(INSTALL) --mode=644 -D 
INSTALL_DIR=$(INSTALL) --directory
INSTALL_PROGRAM=$(INSTALL) --mode=755 -D
MOCK=/usr/bin/mock
RPMLINT=/usr/bin/rpmlint
SHELL=/bin/sh
SRVREPOCTL=srvrepoctl

# Standard Makefile installation directories.
#
# The following are the standard GNU/RPM values which are defined in
# /usr/lib/rpm/macros. See
# http://www.gnu.org/software/make/manual/html_node/Directory-Variables.html
# for more information.
prefix=/usr
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin
datadir=$(prefix)/share
includedir=$(prefix)/include
infodir=$(prefix)/info
libdir=$(exec_prefix)/lib
libexecdir=$(exec_prefix)/libexec
localstatedir=$(prefix)/var
mandir=$(prefix)/man
rpmconfigdir=$(libdir)/rpm
sbindir=$(exec_prefix)/sbin
sharedstatedir=$(prefix)/com
sysconfdir=$(prefix)/etc

# Fedora installation directory overrides.
#
# We override some of the previous GNU/RPM default values with those
# values suiteable for a Fedora/RedHat/CentOS linux system, as defined
# in /usr/lib/rpm/redhat/macros.
#
# This section can be put into a separate file and included here if we
# want to create a more multi-platform Makefile system.
sysconfdir=/etc
defaultdocdir=/usr/share/doc
infodir=/usr/share/info
localstatedir=/var
mandir=/usr/share/man
sharedstatedir=$(localstatedir)/lib


####################
# Makefile targets #
####################

.PHONY: all
all: $(OBJS_ELC)
	$(info all:)

$(OBJS_ELC): $(SRCS_EL)
	$(EMACS) --batch --funcall batch-byte-compile $(SRCS_EL)

.PHONY: lint
lint:
	$(info lint:)
	$(RPMLINT) $(spec_file)

testdestdir:
	mkdir -p 	$(testdestdir)
	$(eval DESTDIR=$(:=$(testdestdir))

.PHONY: testinstall
testinstall: | testdestdir install installcheck

.PHONY: install
install:
	$(info install:)
	$(INSTALL_DATA) -t $(DESTDIR)$(datadir)/emacs/site-lisp/flycheck-fish/ $(name).el $(name).elc
	$(INSTALL_PROGRAM) $(name).fish $(DESTDIR)$(datadir)/emacs/site-lisp/flycheck-fish/bin/$(name)
	$(INSTALL_DATA) -t $(DESTDIR)$(datadir)/emacs/site-lisp/site-start.d/ $(name)-init.el

.PHONY: installcheck
installcheck:
	$(info installcheck:)
	test -r $(DESTDIR)$(datadir)/emacs/site-lisp/flycheck-fish/$(name).el
	test -r $(DESTDIR)$(datadir)/emacs/site-lisp/flycheck-fish/$(name).elc
	test -x $(DESTDIR)$(datadir)/emacs/site-lisp/flycheck-fish/bin/$(name)
	test -r $(DESTDIR)$(datadir)/emacs/site-lisp/site-start.d/$(name)-init.el

.PHONY: uninstall
uninstall:
	$(info uninstall:)
	rm -rf $(DESTDIR)$(datadir)/emacs/site-lisp/flycheck-fish
	rm -f  $(DESTDIR)$(datadir)/emacs/site-lisp/site-start.d/$(name)-init.el

.PHONY: testdist
testdist: 
	mkdir $(dist_name) 
	cp --recursive --target-directory=$(dist_name) $(shell git ls-tree --name-only master)
	tar --create --gzip --file $(tarball) $(dist_name)
	rm -rf $(dist_name)

.PHONY: dist
dist:
	$(GIT) archive --format=tar.gz --prefix=$(dist_name)/ $(dist_tree_ish) > $(tarball)

.PHONY: srpm
srpm: $(tarball)
	$(MOCK) --root=$(MOCKROOT) --resultdir=$(MOCKRESULTDIR) --buildsrpm \
		--spec $(spec_file) --sources $(tarball)

.PHONY: rpm
rpm: srpm
	$(MOCK) --root=$(MOCKROOT) --resultdir=$(MOCKRESULTDIR) --rebuild $(SRPMFILE)

.PHONY: clean
clean:
	$(info clean:)
	rm -f *.rpm *.tar.gz *.elc

.PHONY: distclean
distclean: clean
	$(info distclean:)
	rm -f *.log *~
	rm -rf testdestdir

.PHONY: requirements
requirements:
	sudo $(DNF_INSTALL) $(requirements)

.PHONY: help
help:
	$(info help:)
	$(info Usage: make TARGET [VAR1=VALUE VAR2=VALUE])
	$(info )
	$(info Targets:)
	$(info   all             The default target, bulids binary object files.)
	$(info   lint            Lint the source files.)
	$(info   testinstall     Perform a test installation.)
	$(info   install         Install.)
	$(info   installcheck    Post-installation check of all installed files.)
	$(info   dist		 Create a distribution tarball.)
	$(info   srpm		 Create a SRPM.)
	$(info   rpm		 Create a RPM.)
	$(info   clean           Clean up all generated binary files.)
	$(info   distclean       Clean up all generated files.)
	$(info   help            Display this help message.)
	$(info   printvars       Print variable values (useful for debugging).)
	$(info   printmakevars   Print the Make variable values (useful for debugging).)
	$(info )

.PHONY: printvars
printvars:
	$(info printvars:)
	$(info name=$(name))
	$(info spec_file=$(spec_file))
	$(info version=$(version))
	$(info dist_name=$(dist_name))
	$(info dist_tree_ish=$(dist_tree_ish))
	$(info tarball=$(tarball))
	$(info testdestdir=$(testdestdir))
	$(info requirements=$(requirements))
	$(info DISTRO=$(DISTRO))
	$(info DISTPREFIX=$(DISTPREFIX))
	$(info RELEASEVER=$(RELEASEVER))
	$(info ARCH=$(ARCH))
	$(info MOCKROOT=$(MOCKROOT))
	$(info MOCKRESULTDIR=$(MOCKRESULTDIR))
	$(info RPMFILE=$(RPMFILE))
	$(info SRPMFILE=$(SRPMFILE))
	$(info SRCS_EL=$(SRCS_EL))
	$(info OBJS_ELC=$(OBJS_ELC))
	$(info DNF_INSTALL=$(DNF_INSTALL))
	$(info EMACS=$(EMACS))
	$(info GIT=$(GIT))
	$(info GZIP=$(GZIP))
	$(info INSTALL=$(INSTALL))
	$(info INSTALL_DATA=$(INSTALL_DATA))
	$(info INSTALL_DIR=$(INSTALL_DIR))
	$(info INSTALL_PROGRAM=$(INSTALL_PROGRAM))
	$(info MOCK=$(MOCK))
	$(info RPMLINT=$(RPMLINT))
	$(info SHELL=$(SHELL))
	$(info prefix=$(prefix))
	$(info exec_prefix=$(exec_prefix))
	$(info bindir=$(bindir))
	$(info datadir=$(datadir))
	$(info includedir=$(includedir))
	$(info infodir=$(infodir))
	$(info libdir=$(libdir))
	$(info libexecdir=$(libexecdir))
	$(info localstatedir=$(localstatedir))
	$(info mandir=$(mandir))
	$(info rpmconfigdir=$(rpmconfigdir))
	$(info sbindir=$(sbindir))
	$(info sharedstatedir=$(sharedstatedir))
	$(info sysconfdir=$(sysconfdir))
	$(info defaultdocdir=$(defaultdocdir))
	$(info infodir=$(infodir))
	$(info localstatedir=$(localstatedir))
	$(info mandir=$(mandir))
	$(info sharedstatedir=$(sharedstatedir))

.PHONY: printmakevars
printmakevars:
	$(info printmakevars:)
	$(info $(.VARIABLES))
