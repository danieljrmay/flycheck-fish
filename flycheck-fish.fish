#!/usr/bin/env fish
#
# flycheck-fish.fish
#
# Author: Daniel J. R. May
# Date: 08 December 2020
#
# This script is part of the flycheck-fish package for emacs. It is
# called by the by the fish flycheck-checker which is defined in the
# flycheck-fish.el file.
#

function usage
    echo "Usage:"

    set_color --bold; echo -n "    flycheck-fish "
    set_color normal; echo -n "["
    set_color --bold; echo -n "-h"
    set_color normal; echo -n "|"
    set_color --bold; echo -n "--help"
    set_color normal; echo "]        Print this usage message"
    
    
    set_color --bold; echo -n "    flycheck-fish "
    set_color normal; 
    set_color --underline; echo -n "FILE"
    set_color normal; echo "               Syntax check FILE"

    echo
    echo "This executable script is used by the flycheck-fish package for emacs."
end

# Validate the number of command line arguments, we only accept zero
# or one arguments
switch (count $argv)
    case 0
        usage
        exit 0
    case 1        
        # Expected number of arguments, we continue
        :
    case '*'
        set_color --bold red; echo "Syntax error: flycheck-fish only accepts zero or one arguments."
        set_color normal
        usage
        exit 1
end

switch $argv[1]
    case '-h'
        usage
        exit 0
    case '--help'
        usage
        exit 0
    case '*'
        # We check that the single argument we have been given
        # corresponds to a readable file
        if test -f $argv[1] -a -r $argv[1]
            # Fish-syntax check the file
            fish --no-execute --private $argv[1]

            # We return an exit status of 0, irrespective of what the
            # exit status of the `fish --no-execute ...` command
            # returned.
            exit 0
        else
            set_color --bold red; echo "The path '$argv[1]' does not correspond to a readable file."
            set_color normal
            exit 2
        end
end
