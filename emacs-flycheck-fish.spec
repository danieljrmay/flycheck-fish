%global srcname flycheck-fish

Name:           emacs-%{srcname}
Version:        0.5
Release:        1%{?dist}
Summary:        Flycheck syntax checker for Fish shell code
License:        GPLv3
URL:            https://gitlab.com/danieljrmay/flycheck-fish
Source0:        %{url}/-/archive/%{version}/%{srcname}-%{version}.tar.gz
BuildArch:      noarch
Requires:       emacs emacs-flycheck fish
BuildRequires:  emacs-nox emacs-flycheck make

%description
A very basic syntax checker for fish shell code in Emacs.

%prep
%autosetup -n %{srcname}-%{version}

%build
make

%install
%make_install

%files
%{_datadir}/emacs/site-lisp/%{srcname}
%{_datadir}/emacs/site-lisp/site-start.d/%{srcname}-init.el
%doc README.md TODO.md
%license LICENSE

%changelog
* Fri Feb 18 2022 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.5-1
- Add logos
- Update spec BuildRequires to include make

* Wed Dec  9 2020 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.4-1
- Fixed spec file.

* Wed Dec  9 2020 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.3-1
- Improved documentation, converted to markdown.
- Add help switches and usage messages to fish script.

* Tue Dec  8 2020 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.2-1
- Release as gzipped tarball so it automatically works with GitLab.

* Tue Dec  8 2020 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.1-1
- Initial release.
