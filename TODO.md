# flycheck-fish Todo List #

* [x] Add test scripts and improve error message parsing
* [x] Change dist format to .tar.gz so automatically works with GitLab
* [x] Add RPM build via Copr
* [x] Change todo format to markdown so it works in GitLab better
* [x] Tidy up `flycheck-fish.fish` maybe add a `-|--help` options
* [x] Update README to reflect availability as an RPM
* [x] Add a screenshot image to the README
* [x] Add a project avatar image
* [x] Convert README to MarkDown so we have consistent documentation
      format for the project
* [x] Add GitLab build success badge
* [ ] Release on MELPA?
* [ ] Add automated testing
